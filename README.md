# Albatros

**Project structure**

```javascript

/**
 * Project structure
 */
.
+--albatros
|___
    +--app
        |___+--assets
        |___+--controllers
        |___+--css
        |___+--factories
        |___+--templates
|___
    +--bower_components
        |___+--libraries
|___
    +--rest
        |___+--database
        |___+--services
        |__+--utils
        
```

# Developers
|       Name      |        Status     |
| --------------- |  -----------------|
| Marjo Ballabani | `Project manager` |
| Nedjan Shabani  | `Contact`         |
| Ervis Sharra    |  Developer        |
| Naidi Shutli    |  Developer        |
| Rio Sherri      |  Developer        |