<?php
/**
 * Created by PhpStorm.
 * User: Marjo
 * Date: 10/26/2015
 * Time: 8:50 PM
 */

class DB_CONNECT {
    function __construct() {
        /* Connect to database */
        $this -> connect();
    }

    function __destruct() {
        /* Close connection */
        $this -> close();
    }

    /**
     * Connect with the database
     *
     * @return resource
     */
    function connect() {
        /* Import database variables */
        require_once __DIR__ . '/db-config.php';

        $connection = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());

        /* Select database */
        $database = mysql_select_db(DB_NAME) or die(mysql_error());

        return $connection;
    }

    /**
     * Close connection with database
     */
    function close() {
        /* Close database connection */
        mysql_close();
    }
}
?>