/**
 * Created by Marjo on 11/1/2015.
 */
var app = angular.module('mainApp', ['ngRoute', 'ngDialog']);

app.config(['$routeProvider' ,function($routeProvider){
    $routeProvider
        .when('/dashboard',{
            templateUrl: 'templates/dashboard/dashboard.html',
            controller: 'DashboardCtrl'
        })

}]);